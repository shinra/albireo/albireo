from collections import namedtuple
from enum import IntEnum

Reg = namedtuple('Reg', 'name offset description')
Bit = namedtuple('Bit', 'name bit description')


base_addr = 0xFEB0
high_addr = (base_addr >> 8) & 0xFF
low_addr = base_addr & 0xFF


registers = (
        Reg('RBR', 0, 'Receiver buffer (read, DLAB=0)'),
        Reg('THR', 0, 'Transmitter holding register (write, DLAB=0)'),
        Reg('IER', 1, 'Interrupt enable register (DLAB=0)'),
        Reg('IIR', 2, 'Interrupt identification register (read)'),
        Reg('FCR', 2, 'FIFO control register (write)'),
        Reg('LCR', 3, 'Line control register'),
        Reg('MCR', 4, 'Modem control register'),
        Reg('LSR', 5, 'Line status register'),
        Reg('MSR', 6, 'Modem status register'),
        Reg('SCR', 7, 'Scratch register'),
        Reg('DLL', 0, 'Divisor latch LSB (DLAB=1)'),
        Reg('DLM', 1, 'Divisor latch MSB (DLAB=1)'),
        Reg('EFR', 2, 'Extended functions rgister (LCR=&BF)'),
        )


enums = []


class CEnum(IntEnum):
    @classmethod
    def print(cls):
        max_size = max(len(elem.name) for elem in cls)
        if cls.__doc__:
            print('; {0}'.format(cls.__doc__))
        for elem in cls:
            print(('ACE_{0}_{1.name:<%d}\tEQU {1.value:>4}' % max_size).format(cls.__name__.upper(), elem))


def output(cls):
    if issubclass(cls, CEnum):
        enums.append(cls)
    else:
        raise Exception('Unknown type %s' % type(cls))


@output
class Err(CEnum):
    """Error code"""
    NONE = 0
    NOT_DETECTED = 1

    IN_FIFO = 10

    OVERRUN = 20
    PARITY = 21
    FRAMING = 22
    BREAK = 23


@output
class IER(CEnum):
    """Interrupt Enable Register (IER) bits"""
    ERBI = 0
    ETBEI = 1
    ELSE = 2
    EDSSI = 3


@output
class LCR(CEnum):
    """Line Control Register (LCR) bits"""
    WLS0 = 0
    WLS1 = 1
    STB = 2
    PEN = 3
    EPS = 4
    STICKP = 5
    BREAK = 6
    DLAB = 7

@output
class LSR(CEnum):
    """Line Status Register (LSR) bits"""
    DR = 0
    OE = 1
    PE = 2
    FE = 3
    BI = 4
    THRE = 5
    TEMT = 6
    RCVRFIOERR = 7

@output
class MCR(CEnum):
    """Modem Control Register (MCR) bits"""
    DTR = 0
    RTS = 1
    OUT1 = 2
    OUT2 = 3
    LOOP = 4
    AFE = 5

    
print('; Register base address')
print('ACE_ADDR\tEQU {addr:<#6x}\t; IO base address'.format(addr=base_addr))
print('ACE_ADDRh\tEQU {addr:<#6x}\t; IO base address'.format(addr=high_addr))
print('ACE_ADDRl\tEQU {addr:<#6x}\t; IO base address'.format(addr=low_addr))

print('; Register offsets')
for reg in registers:
    print('ACE_{name}_OFF\tEQU {offset:<6}\t; {description}'.format(**reg._asdict()))

print('; Register address')
for reg in registers:
    print('ACE_{name}\t\tEQU ACE_ADDR+ACE_{name}_OFF'.format(**reg._asdict()))

print('; Register address (high/low bits separeted)')
for reg in registers:
    print('ACE_{name}h\tEQU ACE_ADDRh'.format(**reg._asdict()))
    print('ACE_{name}l\tEQU ACE_ADDRl+ACE_{name}_OFF'.format(**reg._asdict()))


for enum in enums:
    enum.print()
