    include 'text.macros.z80'
    include 'xmodem.macros.z80'
    include 'sio.macros.z80'

    GLOBAL  ReceiveYMODEM

YM_INFO_RECV_STATE          equ 0
YM_INFO_RECV_STATE_COUNTER  equ 1
YM_INFO_EVENTBLOCK          equ 2 ; +13
YM_INFO_EVENTBLOCK_TICK_L   equ 4
YM_INFO_EVENTBLOCK_TICK_H   equ 5
YM_INFO_EVENTBLOCK_RELOAD_L equ 6
YM_INFO_EVENTBLOCK_RELOAD_H equ 7
YM_INFO_2K_BUFF_L           equ 15
YM_INFO_2K_BUFF_H           equ 16
YM_INFO_RECV_BUFF_START_L   equ 17
YM_INFO_RECV_BUFF_START_H   equ 18
YM_INFO_RECV_BUFF_SIZE_L    equ 19
YM_INFO_RECV_BUFF_SIZE_H    equ 20
YM_INFO_RECV_BUFF_CURR_L    equ 21
YM_INFO_RECV_BUFF_CURR_H    equ 22
YM_INFO_FILESIZE            equ 23 ; +4
YM_INFO_FILESIZE_L          equ YM_INFO_FILESIZE+0
YM_INFO_FILESIZE_H          equ YM_INFO_FILESIZE+1
YM_INFO_FILESIZE_E          equ YM_INFO_FILESIZE+2
YM_INFO_FILESIZE_D          equ YM_INFO_FILESIZE+3
YM_INFO_FILENAME            equ 27 ; +12
YM_INFO_BLOCK_INDEX         equ 39
YM_INFO_BLOCK_CRC1          equ 40
YM_INFO_BLOCK_CRC2          equ 41

YM_STATE_NONE       equ 0
YM_STATE_WAIT_START equ 1
YM_STATE_WAIT_BLOCK equ 2

; List of states
;WAIT_START
;WAIT_BLOCK
;WAIT_METADATA
;WAIT_DATA


    macro   m_set_state,state,timer,counter
        LD  (IX+YM_INFO_RECV_STATE),\state                  ;   6µs
        LD  (IX+YM_INFO_EVENTBLOCK_TICK_H),50*\timer/256    ;   6µs
        LD  (IX+YM_INFO_EVENTBLOCK_TICK_L),50*\timer%256    ;   6µs
        LD  (IX+YM_INFO_EVENTBLOCK_RELOAD_H),50*\timer/256  ;   6µs
        LD  (IX+YM_INFO_EVENTBLOCK_RELOAD_L),50*\timer%256  ;   6µs
        LD  (IX+YM_INFO_RECV_STATE_COUNTER),\counter        ;   6µs
    endmacro

    macro   m_get_block_index
        ; Get block index
        ;SIO_READ_1_BYTE_POLL_BLOCK
        CALL    SIO_READ_1_BYTE_POLL
        LD  (IX+YM_INFO_BLOCK_INDEX),A
        ;SIO_READ_1_BYTE_POLL_BLOCK
        CALL    SIO_READ_1_BYTE_POLL
    endmacro 

;   TODO:
;   Timeout and retry
;   Some buffer purging before sending request
;   Support stream mode ('G' start chr, only final ACK)
;   Processing received filename for creating a short size filename ?
;   RSX and ROM
;   We should now the accepted filename size by the current drive before error
ReceiveYMODEM:

;    CP  1
;    JP  NZ,no_filename_err

    LD  HL,1   ; 1500000 baud
    CALL    SIO_INIT
    JP  NC,sio_init_err

    ; Get freespace
    ; Addr are not the same on 464 (firmware v1) so we need to detect if it.
    CALL    _KL_L_ROM_ENABLE
    PUSH    AF
    LD  A,(&681)    ; 464 firmware contains "(x1)" at &681, with x depending of the firmware language
    CP  '('
    JP  NZ,.not464
    LD  A,(&683)
    CP  '1'
    JP  NZ,.not464
    LD  A,(&684)
    CP  ')'
    JP  NZ,.not464
.464
    puts    t_464_detected
    LD  DE,(&AE89)  ; Start of freespace
    LD  HL,(&B08D)  ; End of freespace
    JP  .enddetect
.not464
    puts    t_not_464_detected
    LD  DE,(&AE6C)  ; Start of freespace (AE89 on 464)
    LD  HL,(&B071)  ; End of freespace (B08D on 464)
.enddetect
    POP AF
    CALL    _KL_ROM_RESTORE

    ; Allocate memory for meta
    OR  A
    LD  BC,42
    SBC HL,BC
    EX  DE,HL
    LD  IXh,D
    LD  IXl,E
    EX  DE,HL

    ; Receiving state machine
    m_set_state YM_STATE_NONE,0,0

    ; Allocate 2K buffer for DOS buffer
    ; HL -= 2048
    LD  A,H
    SUB 2048/256
    LD  H,A

    LD  (IX+YM_INFO_2K_BUFF_H), H
    LD  (IX+YM_INFO_2K_BUFF_L), L

    ; File buffer 
    CALL    ALIGNHL1024low
    CALL    ALIGNDE1024high

    LD  (IX+YM_INFO_RECV_BUFF_START_H), D
    LD  (IX+YM_INFO_RECV_BUFF_START_L), E
    LD  (IX+YM_INFO_RECV_BUFF_CURR_H), D
 
    LD  (IX+YM_INFO_RECV_BUFF_CURR_L), E

    print_hexa_16
    putchar ' '
    EX  DE,HL
    print_hexa_16
    putchar ' '
    EX  DE,HL

    ; Calc buffer size
    OR  A
    SBC HL,DE
    INC HL

    LD  (IX+YM_INFO_RECV_BUFF_SIZE_H), H
    LD  (IX+YM_INFO_RECV_BUFF_SIZE_L), L

    ; Be sure to empty buffer before starting
.clean_in_buff
    CALL    SIO_READ_1_BYTE_NONBLOCK
    JP  C,.clean_in_buff
    
;   CALL    INIT_TIMEOUT_EVENTBLOCK

    puts    t_waiting

    DI

    JP  .request_transmission

    RET

;.test
;    LD  HL,ymeta+3
;    CALL    PROCESS_METABLOCK
;
;    CALL    _KM_WAIT_CHAR
;
;    CALL    PRINT_RECEIVING
;    
;    CALL    _KM_WAIT_CHAR
;
;    LD  D,IXh
;    LD  E,IXl
;    LD  BC,YM_INFO_FILENAME
;    EX  DE,HL
;    ADD HL,BC
;    PUSH    HL  ; filename addr
;
;    LD  B,0 ; filename size
;.filename_len_loop_test
;    LD  A,(HL)
;    OR  A
;    JP  Z,.filename_len_loop_end_test
;    INC HL
;    INC B
;    LD  A,12
;    SUB A,B
;    JP  NZ,.filename_len_loop_test
;.filename_len_loop_end_test
;
;    LD  D,(IX+YM_INFO_2K_BUFF_H)
;    LD  E,(IX+YM_INFO_2K_BUFF_L)
;    POP HL  ; filename addr
;
;    PUSH    IX
;;    CALL    _CAS_OUT_OPEN
;    ;   TODO Detect open error
;    POP IX
;
;    LD  H,(IX+YM_INFO_RECV_BUFF_START_H)
;    LD  L,(IX+YM_INFO_RECV_BUFF_START_L)
;    LD  D,(IX+YM_INFO_FILESIZE_H)
;    LD  E,(IX+YM_INFO_FILESIZE_L)
;    LD  BC,0
;    LD  A,%00010110
;;    CALL    _CAS_OUT_DIRECT
    ;   TODO Detect error

;    CALL    _CAS_OUT_CLOSE
    

    RET

    LD  HL,1 
    CALL    SIO_INIT

    CALL    _KM_WAIT_CHAR
    putcharA

    CALL    _KM_WAIT_CHAR
    putcharA


    JP  NC,sio_init_err
    RET

;    LD  L,(IX+0)
;    LD  H,(IX+1)

;    LD  A,12
;    LD  C,(HL)  ; size
;    SUB C
;    JP  M,filename_size_err

;    INC HL
;    LD  E,(HL)
;    INC HL
;    LD  D,(HL)

    puts t_ready

;    LD  H,D
;    LD  L,E
;
;    LD  B,C
;.filenameloop
;    LD  A,(HL)
;    CALL    _TXT_OUTPUT
;    INC HL
;    DJNZ    .filenameloop

;    puts    t_crlf

    puts    t_waitkey

    CALL    _KM_WAIT_CHAR

;    EX  DE,HL
;    LD  B,C
;
;    LD  DE,buffer
;    CALL    _CAS_OUT_OPEN

.request_transmission
    ; Send transmision start 
    m_set_state YM_STATE_WAIT_START,10,3
 
    LD  A,'C'
    CALL    SIO_WRITE_1_BYTE_POLL   ; corrup F, BC, D
    CALL    _TXT_OUTPUT

.wait_block_start:
    CALL    SIO_READ_1_BYTE_POLL
    print_hexa D
    LD  A,D
    CP  STX
    JP  Z,.receive_1024_block
    CP  SOH
    JP  Z,.receive_128_block
    CP  EOT
    JP  Z,.receive_end_block
    JP  NZ,.wait_block_start

.metadata_block
    puts    t_metablock

    ; Get block payload
    LD  D,(IX+YM_INFO_RECV_BUFF_CURR_H)                 ;   6µs
    LD  E,(IX+YM_INFO_RECV_BUFF_CURR_L)                 ;   6µs
    LD  HL,128
    CALL    SIO_READ_N_BYTES_POLL

    ; Get block CRC
    CALL    SIO_READ_1_BYTE_POLL
    CALL    SIO_READ_1_BYTE_POLL
;    LD  DE,crc
;    LD  HL,2
;    CALL    SIO_READ_N_BYTES_POLL

    LD  H,(IX+YM_INFO_RECV_BUFF_CURR_H)
    LD  L,(IX+YM_INFO_RECV_BUFF_CURR_L)
;    PUSH    HL

    putchar '4'

    CALL    PROCESS_METABLOCK
    
    putchar '5'

    CALL    PRINT_RECEIVING


    ; Get filename addr
    LD  D,IXh
    LD  E,IXl
    LD  BC,YM_INFO_FILENAME
    EX  DE,HL
    ADD HL,BC
    PUSH    HL  ; filename addr

    LD  B,0 ; filename size
.filename_len_loop
    LD  A,(HL)
    OR  A
    JP  Z,.filename_len_loop_end
    INC HL
    INC B
    LD  A,12
    SUB A,B
    JP  NZ,.filename_len_loop
.filename_len_loop_end

    LD  D,(IX+YM_INFO_2K_BUFF_H)
    LD  E,(IX+YM_INFO_2K_BUFF_L)
    POP HL  ; filename addr

    PUSH    IX
    CALL    _CAS_OUT_OPEN
    ;   TODO Detect error
    POP IX

    JP  C,.open_no_error
    puts    t_error
.open_no_error

    send_ack

    JP  .request_transmission


    ; Test file size
    LD  A,(IX+YM_INFO_FILESIZE_D)
    LD  B,(IX+YM_INFO_FILESIZE_E)
    OR  B
    JP  NZ,.tobig

    LD  A,(IX+YM_INFO_RECV_BUFF_SIZE_H)
    LD  B,(IX+YM_INFO_FILESIZE_H)
    CP  B
    JP  M,.tobig
    
.tobig
    NOP    

    putchar 'H'
    putchar 'e'
    putchar 'r'
    putchar 'e'


    ; Increase buffer pointer by block size
;    POP HL
;    LD  DE,128
;    ADD HL,DE
;    LD  (IX+YM_INFO_RECV_BUFF_CURR_H),H
;    LD  (IX+YM_INFO_RECV_BUFF_CURR_L),L

;    LD  HL,block
;    LD  DE,filename
;    LD  C,0
;.extract_filename_loop
;    LD  A,(HL)
;    OR  A
;    JP  Z,.extract_filename_end
;    LD  (DE),A
;    INC C
;    LD  A,12
;    SUB C
;    JP  M,filename_size_err
;.extract_filename_end

 ;   CP  12  ; filesize = 0, no more file
 ;   JP  Z,.no_more_file

 ;   LD  B,C ; filename size
 ;   LD  HL,filename
;.filenameloop
;    LD  A,(HL)
;    CALL    _TXT_OUTPUT
;    INC HL
;    DJNZ    .filenameloop

;    LD  B,C ; filename size
;    LD  HL,filename
;    LD  DE,buffer
;    CALL    _CAS_OUT_OPEN

    LD  D,IXh
    LD  E,IXl
    LD  BC,YM_INFO_FILENAME
    EX  DE,HL
    ADD HL,BC
    PUSH    HL  ; filename addr

    LD  B,0 ; filename size
.filename_len_loop_2
    LD  A,(HL)
    OR  A
    JP  Z,.filename_len_loop_end
    INC HL
    INC B
    LD  A,12
    SUB A,B
    JP  NZ,.filename_len_loop
.filename_len_loop_end_2

    LD  D,(IX+YM_INFO_2K_BUFF_H)
    LD  E,(IX+YM_INFO_2K_BUFF_L)
    POP HL  ; filename addr

    PUSH    IX
;    CALL    _CAS_OUT_OPEN
    ;   TODO Detect error
    POP IX

    send_ack

    JP  .request_transmission

.receive_128_block
;    puts    t_get_soh
;    putchar ' '

    m_set_state YM_STATE_WAIT_BLOCK,1,5

    ; Get block index
    ; m_get_block_index
    CALL    SIO_READ_1_BYTE_POLL
    LD  (IX+YM_INFO_BLOCK_INDEX),A
    print_hexa D
    CALL    SIO_READ_1_BYTE_POLL
    print_hexa D
    LD  A,(IX+YM_INFO_BLOCK_INDEX)
    ADD A,D
    INC A

    JP NZ,.wait_block_start
    putchar '!'

    LD  A,(IX+YM_INFO_BLOCK_INDEX)
    OR  A
    JP  Z,.metadata_block

;    puts    t_curr_block
;    LD  A,(IX+YM_INFO_BLOCK_INDEX)
;    print_hexa_h
;    LD  A,(IX+YM_INFO_BLOCK_INDEX)
;    print_hexa_l
;    crlf

    ; Get block payload
;   LD  DE,block
;   LD  HL,128
;   CALL    SIO_READ_N_BYTES_POLL
    PUSH    IX  ; _CAS_OUT_CHAR corrup IX
    LD  D,128
.write_loop1
    CALL    SIO_READ_1_BYTE_POLL
    CALL    _CAS_OUT_CHAR
    DEC D
    JP  NZ,.write_loop1
    POP     IX  ; _CAS_OUT_CHAR corrupt IX

    ; Get block CRC
    LD  DE,crc
    LD  HL,2
    CALL    SIO_READ_N_BYTES_POLL

    send_ack

    JP  .wait_block_start

.receive_1024_block
;    puts    t_get_stx
;    putchar ' '

    m_set_state YM_STATE_WAIT_BLOCK,1,5

    ; Get block index
    CALL    SIO_READ_1_BYTE_POLL
    LD  (IX+YM_INFO_BLOCK_INDEX),A
    print_hexa D
    CALL    SIO_READ_1_BYTE_POLL
    print_hexa D
    LD  A,(IX+YM_INFO_BLOCK_INDEX)
    ADD A,D
    INC A

    JP NZ,.wait_block_start
    putchar '!'

    LD  A,(IX+YM_INFO_BLOCK_INDEX)
    OR  A
    JP  Z,.metadata_block

;    puts    t_curr_block
;    LD  A,(IX+YM_INFO_BLOCK_INDEX)
;    print_hexa_h
;    LD  A,(IX+YM_INFO_BLOCK_INDEX)
;    print_hexa_l
;    crlf

    PUSH    IX  ; _CAS_OUT_CHAR corrupt IX
    LD  D,0 ; pourquoi D et pas B ?!?
.write_loop2
    CALL    SIO_READ_1_BYTE_POLL
    CALL    _CAS_OUT_CHAR
    CALL    SIO_READ_1_BYTE_POLL
    CALL    _CAS_OUT_CHAR
    CALL    SIO_READ_1_BYTE_POLL
    CALL    _CAS_OUT_CHAR
    CALL    SIO_READ_1_BYTE_POLL
    CALL    _CAS_OUT_CHAR
    DEC D
    JP  NZ,.write_loop2
    POP     IX  ; _CAS_OUT_CHAR corrupt IX

    ; Get block CRC
    LD  DE,crc
    LD  HL,2
    CALL    SIO_READ_N_BYTES_POLL

    send_ack

    JP  .wait_block_start

.receive_end_block
    puts    t_get_eot

    send_ack

    puts    t_last_block
    LD  A,(IX+YM_INFO_BLOCK_INDEX)
    print_hexa  B
    puts    t_crlf
    puts    t_finished

    PUSH    IX
    CALL    _CAS_OUT_CLOSE
    POP     IX

    JP  C,.close_no_error
    puts    t_error
    print_hexa B
.close_no_error

    JP  .request_transmission

.no_more_file
    RET


PRINT_RECEIVING:
;   Corrupts:   AF, BC, HL
    puts t_receiving
    LD  D,IXh
    LD  E,IXl
    LD  BC,YM_INFO_FILENAME
    EX  DE,HL
    ADD HL,BC
    CALL    PRINT
    puts    t_crlf
    EX  DE,HL
    RET


PROCESS_METABLOCK:
;   INPUT:  HL metablock start address
;   Corrupts:   AF, BC, DE, HL
;   Block 0 with metadata in the YMODEM batch protocol
;   Contains:
;       Pathname in null terminated ASCII string
;   TODO: full path is not supported, detect and gracefully sned error
;       Length in decimal string
;       Modification Date in octal ASCII string, in second since 1970-01-01T00:00:00Z
;       Mode in octal ASCII string
;       Serial Number in octal ASCII string
;       Nb file left (including this one) in decimal ASCII string (lrzsz extension ?)
;       Total length left (other batch file include) in decimal ASCII string (lrzsz extension ?)
    LD  D,IXh
    LD  E,IXl
    EX  DE,HL
    LD  BC,YM_INFO_FILENAME
    ADD HL,BC
    EX  DE,HL
    LD  C,0

.extract_filename_loop
    LD  A,(HL)
    putcharA
    INC HL
    LD  (DE),A
    INC DE
    OR A   ; null terminated string
    JP  Z,.extract_filename_end
    INC C
    LD  A,12
    SUB C
    JP  M,filename_size_err
    JP  .extract_filename_loop
.extract_filename_end

;   TODO: If filename lenth = 0 -> End of transmission
   
   putchar '-' 
;    CALL    _KM_WAIT_CHAR

    PUSH    HL
    PUSH    BC
    EX  DE,HL
    LD  D,IXh
    LD  E,IXl
    LD  BC,YM_INFO_FILENAME
    EX  DE,HL
    ADD HL,BC
    CALL    PRINT
    POP BC
    POP HL

    print_hexa_16
    putchar ' '

;    CALL    _KM_WAIT_CHAR

    LD  B,H
    LD  C,L
    LD  DE,0
    LD  HL,0
    CALL    ascii_decimal_to_hlde

    LD  (IX+YM_INFO_FILESIZE_L),L
    LD  (IX+YM_INFO_FILESIZE_H),H
    LD  (IX+YM_INFO_FILESIZE_E),E
    LD  (IX+YM_INFO_FILESIZE_D),D

    putchar 'F'
    putchar 'i'
    putchar 'l'
    putchar 'e'
    putchar 's'
    putchar 'i'
    putchar 'z'
    putchar 'e'
    putchar ':'
    putchar ' '

    EX  DE,HL
    print_hexa_16
    EX  DE,HL
    print_hexa_16

    crlf

    RET

    macro   dehl_by_two
        ADD HL,HL
        EX  DE,HL
        ADC HL,HL
        EX  DE,HL
    endmacro


ascii_decimal_to_hlde 
; ---------------------------------------------------------------------
; Read HL bytes from serial interface thru FIFO state
;   INPUT:  BC pointer to string to convert
;   OUTPUT: DEHL 32 bits results
;           BC pointer to the last char after last digit
;   Corrupts:   AF
; ---------------------------------------------------------------------
;   Borrow some code from z88dk atol but simplify some part
;   Doesn't support sign
; ---------------------------------------------------------------------
.loop
    LD  A,(BC)
    INC BC

    ; test if digit
    SUB '0'
    RET C
    CP  10
    RET NC
    
    ; DEHL *= 10
    dehl_by_two ;   dehl = in*2
    PUSH    DE
    PUSH    HL  ;   save in*2
    dehl_by_two ;   dehl = in*4
    ADD HL,HL
    EX  DE,HL
    ADC HL,HL   ;   hlde = in*8
    EX  (SP),HL
    ADD HL,DE
    POP DE
    EX  (SP),HL
    ADC HL,DE
    EX  DE,HL
    POP HL

    ADD A,L
    LD  L,A ;   add A to L
    JP  NC,.loop
    INC H   ;   if carry increase H
    JP  NZ,.loop
    INC DE  ;   if carry increase DE

    JP  .loop


ALIGNHL1024high:
; ---------------------------------------------------------------------
; Align HL adresse to next 1024 boundary
;   INPUT:  HL memory address
;   OUTPUT: HL memory address aligned on 1024 boundary
;   Corrupts:   AF
; ---------------------------------------------------------------------
    ; test if already on 1024 low boundary
    XOR A
    CP  L
    LD  L,A ; A contains 0
    JP NZ,.next1024 ; not aligned
    LD  A,H
    AND %00000011
    JP  Z,.aligned ; already aligned
.next1024
    LD  A,H
    AND %11111100
    ADD A,4
    LD  H,A
.aligned
    RET


ALIGNDE1024high:
; ---------------------------------------------------------------------
; Align DE adresse to next 1024 boundary
;   INPUT:  DE memory address
;   OUTPUT: DE memory address aligned on 1024 boundary
;   Corrupts:   AF
; ---------------------------------------------------------------------
    ; test if already on 1024 low boundary
    XOR A
    CP  E
    LD  E,A ; A contains 0
    JP NZ,.next1024 ; not aligned
    LD  A,D
    AND %00000011
    JP  Z,.aligned ; already aligned
.next1024
    LD  A,D
    AND %11111100
    ADD A,4
    LD  D,A
.aligned
    RET

 
ALIGNHL1024low:
; ---------------------------------------------------------------------
; Align HL adresse to previous 1024 boundary
;   INPUT:  HL memory address
;   OUTPUT: HL memory address aligned on 1024 boundary
;   Corrupts:   AF
; ---------------------------------------------------------------------
    INC HL
    LD  A,H
    AND %11111100
    LD  H,A
    LD  L,0
    DEC HL
    RET


INIT_TIMEOUT_EVENTBLOCK:
    PUSH    IX
    POP HL
    LD  DE,YM_INFO_EVENTBLOCK+6
    ADD HL,DE
    LD  DE,TIMEOUT_DETECTION
 
    CALL    _KL_CURR_SELECTION
    LD  C,A
    LD  B,%10000010
    CALL    _KL_INIT_EVENT

    PUSH    IX
    POP HL
    LD  DE,YM_INFO_EVENTBLOCK
    ADD HL,DE
    LD  DE,1
    LD  BC,50
    CALL    _KL_ADD_TICKER

    RET

 
TIMEOUT_DETECTION:
;   Called by the kernel to detect if timeout occur
    DI
;    CALL    _KM_READ_CHAR
;    JP  NC,.test_state
;    CP  3
;    JP  Z,.abort
;    CALL    _KM_CHAR_RETURN
.test_state
    LD  A,(IX+YM_INFO_RECV_STATE)
    CP  YM_STATE_NONE
    JP  Z,.none
    CP  YM_STATE_WAIT_START
    JP  Z,.wait_start
    JP  .default
.none    
;    putchar '.'
    RET
.wait_start
    DEC (IX+YM_INFO_RECV_STATE_COUNTER)
    JP  Z,.abort
    ; TODO: abort
;    putchar '-'
    LD  A,'C'
    CALL    SIO_WRITE_1_BYTE_POLL   ; corrup F, BC, D
    RET
.default
    DEC (IX+YM_INFO_RECV_STATE_COUNTER)
;    putchar '!'
    RET NZ
    LD  (IX+YM_INFO_RECV_STATE),YM_STATE_NONE
    RET
.abort
    puts    t_abort
    RET
    ;   HOWTO ?
    
sio_init_err:
    puts    t_sio_init_err
    RET
no_filename_err:
    puts    t_welcome
    puts    t_no_filename_err
    RET
filename_size_err :
    puts    t_filename_size_err 
    RET

crc:
    defs    2

t_welcome   string  "Simple (pseudo) YMODEM file receive", $d, $a
t_464_detected  string  "464 detected.", $d, $a
t_not_464_detected  string  "Not 464 detected.", $d, $a

ymeta:
    incbin 'ymeta.data'
