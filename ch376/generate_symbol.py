from collections import namedtuple
from enum import IntEnum

Reg = namedtuple('Reg', 'name offset description')
Bit = namedtuple('Bit', 'name bit description')


base_addr = 0xFE80
high_addr = (base_addr >> 8) & 0xFF
low_addr = base_addr & 0xFF


registers = (
        Reg('DATA', 0, 'Data port'),
        Reg('CMD', 1, 'Command port (write)'),
        Reg('STATUS', 1, 'Status port (read)'),
        )


enums = []


class CEnum(IntEnum):
    @classmethod
    def print(cls):
        max_size = max(len(elem.name) for elem in cls)
        if cls.__doc__:
            print('; {0}'.format(cls.__doc__))
        for elem in cls:
            print(('FMC_{0}_{1.name:<%d}\tEQU {1.value:>4}' % max_size).format(cls.__name__.upper(), elem))


def output(cls):
    if issubclass(cls, CEnum):
        enums.append(cls)
    else:
        raise Exception('Unknown type %s' % type(cls))

@output
class Cmd(CEnum):
    """CH376 commands list"""
    GET_IC_VER = 0x01
    SET_BAUDRATE = 0x02
    ENTER_SLEEP = 0x03
    RESET_ALL = 0x05
    CHECK_EXIST = 0x06
    SET_SD0_INT = 0x0B
    GET_FILE_SIZE = 0x0C
    SET_USB_MODE = 0x15
    GET_STATUS = 0x22
    RD_USB_DATA0 = 0x27
    WR_USB_DATA = 0x2C
    WR_REQ_DATa = 0x2D
    WR_OFS_DATA = 0x2E
    SET_FILE_NAME = 0x2F
    DISK_CONNECT = 0x30
    DISK_MOUNT = 0x31
    FILE_OPEN = 0x32
    FILE_ENUM_GO = 0x33
    FILE_CREATE = 0x34
    FILE_ERASE = 0x35
    FILE_CLOSE = 0x36
    DIR_INFO_READ = 0x37
    DIR_INFO_SAVE = 0x38
    BYTE_LOCATE = 0x39
    BYTE_READ = 0x3A
    BYTE_RD_GO = 0x3B
    BYTE_WRITE = 0x3C
    BYTE_WR_GO = 0x3D
    DISK_CAPACITY = 0x3E
    DISK_QUERY = 0x3F
    DIR_CREATE = 0x40
    SEG_LOCATE = 0x4A
    SEC_READ = 0x4B
    SEC_WRITE = 0x4C
    DISK_BOC_CMD = 0x50
    DISK_READ = 0x54
    DISK_RD_GO = 0x55
    DISK_WRITE = 0x56
    DISK_WR_GO = 0x57




@output
class Err(CEnum):
    """Error code"""
    NONE = 0
    NOT_DETECTED = 1

print('; Register base address')
print('FMC_ADDR\tEQU {addr:<#6x}\t; IO base address'.format(addr=base_addr))
print('FMC_ADDRh\tEQU {addr:<#6x}\t; IO base address'.format(addr=high_addr))
print('FMC_ADDRl\tEQU {addr:<#6x}\t; IO base address'.format(addr=low_addr))

print('; Register offsets')
for reg in registers:
    print('FMC_{name}_OFF\tEQU {offset:<6}\t; {description}'.format(**reg._asdict()))

print('; Register address')
for reg in registers:
    print('FMC_{name}\t\tEQU FMC_ADDR+FMC_{name}_OFF'.format(**reg._asdict()))

print('; Register address (high/low bits separeted)')
for reg in registers:
    print('FMC_{name}h\tEQU FMC_ADDRh'.format(**reg._asdict()))
    print('FMC_{name}l\tEQU FMC_ADDRl+FMC_{name}_OFF'.format(**reg._asdict()))


for enum in enums:
    enum.print()
