include common.mk
SUBDIRS=albireo sio ch376 ymodem misc

CLEANDIRS = $(SUBDIRS:%=clean-%)
MRPROPERDIRS = $(SUBDIRS:%=mrproper-%)

#all: test.dsk bootstrap.bin xmodem.bin ymodem.bin ymodem.rom albidiag.rom
all: bootstrap.bin xmodem.bin ymodem.bin ymodem.rom albidiag.rom albidiag.bin

test.dsk: thex.bin tterm.bin tsio.bin tch376.bin xmodem.bin ymodem.bin YMODEM.BAS albidiag.bin
	cpcfs test.dsk f
	cpcfs test.dsk p thex.bin
	cpcfs test.dsk p tterm.bin
	cpcfs test.dsk p tsio.bin
	cpcfs test.dsk p tch376.bin
	cpcfs test.dsk p xmodem.bin
	cpcfs test.dsk p ymodem.bin
	cpcfs test.dsk p YMODEM.BAS

thex.bin: test_hex.o sio.o albireo.o text.o
tterm.bin: test_term.o sio.o albireo.o text.o
tsio.bin: test_sio.o sio.o albireo.o text.o
tch376.bin: test_ch376.o ch376.o text.o
bootstrap.bin: bootstrap.o sio.o albireo.o
xmodem.bin: xmodemram.o xmodem.o ymmsg.o sio.o albireo.o text.o xycommon.o
ymodem.bin: ymodemram.o ymodem.o ymmsg.o sio.o albireo.o text.o xycommon.o

ymodem.rom: ymodemrom.o ymodem.o xmodem.o ymmsg.o sio.o albireo.o text.o xycommon.o

albidiag.bin: albidiag.o text.o
albidiag.rom: albidiagrom.o albidiag.o text.o

%.rom:
	vlink -sd -sc -M -Ttext 0xC000 -Trom.ld -b rawbin1 -o $@ $^ > $@.map

%.bin:
	vlink -sd -sc -M -Ttext 0x6000 -Tlink.ld -b amsdos -o $@ $^ > $@.map

bootstrap.o: bootstrap/bootstrap.z80 sio/sio.sym
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

ymmsg.o: ymodem/ymmsg.z80
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

xycommon.o: ymodem/xycommon.z80
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

xmodem.o: ymodem/xmodem.z80 sio/sio.sym
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

ymodem.o: ymodem/ymodem.z80 sio/sio.sym
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

xmodemram.o: ymodem/xmodemram.z80
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

ymodemram.o: ymodem/ymodemram.z80
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

ymodemrom.o: ymodem/ymodemrom.z80
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

test_hex.o: test/test_hex.z80 test/test.z80 sio/sio.sym
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

test_term.o: test/test_term.z80 test/test.z80 sio/sio.sym
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

test_sio.o: test/test_sio.z80 test/test.z80 sio/sio.sym
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

test_ch376.o: test/test_ch376.z80 test/test.z80 ch376/ch376.sym
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

sio.o: sio/sio.z80 sio/sio.sym
	$(AS) $(ASFLAGS) $(INCLUDES) -L $@.lst -o $@ $<

albireo.o: albireo/albireo.z80 albireo/albireo.sym sio/sio.sym
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

ch376.o: ch376/ch376.z80 ch376/ch376.sym
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

text.o: misc/text.z80
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

albidiag.o: diagrom/albidiag.z80
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

albidiagrom.o: diagrom/albidiagrom.z80
	$(AS) $(ASFLAGS) $(INCLUDES) -o $@ $<

symbols: sio/sio.sym ch376/ch376.sym

sio/sio.sym: sio
ch376/ch376.sym: ch376

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

clean: $(CLEANDIRS)
	rm -f *.o
	rm -f *.bin.map
	rm -f *.rom.map
	rm listing

$(CLEANDIRS):
	$(MAKE) -C $(@:clean-%=%) clean

mrproper: $(MRPROPERDIRS) clean
	rm -f *.dsk
	rm -f *.bin
	rm -f *.rom
	rm -f *.lst
$(MRPROPERDIRS):
	$(MAKE) -C $(@:mrproper-%=%) mrproper

.PHONY: subdirs $(SUBDIRS) $(CLEANDIRS) $(MRPROPERDIRS)
.PHONY: clean mrproper
