#!/usr/bin/env python3
import serial
import xmodem

start = None

def getc(size, timeout=1):
#    ser.setTimeout(timeout)
    data = ser.read()
    print('IN: %s' % list(bytearray(data)))
    return data or None

def putc(data, timeout=1):
#    ser.setTimeout(timeout)
    print('OUT: %s' % list(bytearray(data)))
    size = ser.write(data)
    return size or None

def callback(*args, **kwargs):
    global start
    if not start:
        start = time.time()
    print(args, kwargs)

if __name__ == '__main__':
    import sys
    import time
    ser = serial.Serial('/dev/ttyUSB0', baudrate=1500000, rtscts=True)
    #ser = serial.Serial('/dev/ttyUSB0', baudrate=115384, rtscts=True)

    xm = xmodem.XMODEM1k(getc, putc)

    assert len(sys.argv) == 2, "missing filename"

    with open(sys.argv[1], 'rb') as f:
        xm.send(f, callback=callback)
        stop = time.time()
        size = f.tell()
        print('Speed: %f octets per second' % (size / (stop - start)))
